function musicSalle(){
	var MusicRecommender = {
		
		info:'',
		songURL:[],
		songName:[],
		artistID:[],
		
		getInfo : function(){
			return this.info;
		},
		
		getSongURL : function(){
			return this.songURL;
		},
		
		getSongName : function(){
			return this.songName;
		},
		
		getArtistID : function(){
			return this.artistID;
		},
		
		getType : function(){
			if(document.getElementById("cancion").checked == true){
				return document.getElementById("cancion").value;
			}
			if(document.getElementById("artista").checked == true){
				return document.getElementById("artista").value;
			}
			if(document.getElementById("album").checked == true){
				return document.getElementById("album").value;
			}
		},
		
		search : function(buscador, tipo){
			var xhr = new XMLHttpRequest();
			xhr.open("GET", "https://api.spotify.com/v1/search?q="+buscador+"&type="+tipo, false);
			//xhr.open("GET", "https://api.spotify.com/v1/search?q=fran%20perea&type=track", false);
			xhr.send();
			var json = xhr.responseText;
			this.info = JSON.parse(json);
		},
		
		list : function(){
			
			//var a = document.getElementById("listaCanciones");
			var a = document.createElement('ul');
			a.setAttribute("id", "lista");
			
			if(document.getElementById("cancion").checked == true){
				//guardamos cuantos elementos hay en el json recibido
				this.numItems = this.info.tracks.items.length;
				
				//por cada elemento creamos el html para ir añadiendolo
				for(var i=0; i < this.numItems; i++){

					var trackName = this.info.tracks.items[i].name;
					for(var j=0; j< this.info.tracks.items[i].artists.length;j++){
						trackName += " - " + this.info.tracks.items[i].artists[j].name;
					}
					a.innerHTML += "<div class='item'><li id='item" + i + "'>" + trackName + "</li> <i class='fa fa-youtube-play' aria-hidden='true' fa-3x></i> </div>";
					//guardamos el id del album al cual pertenece cada una de las canciones mostradas
					//this.albumID[i] = this.info.tracks.items[i].album.id;
					//guardamos cada uno de los preview de las canciones
					this.songURL[i] = this.info.tracks.items[i].preview_url;
					//guardamos el id de cada cancion
					//this.songIds[i] = this.info.tracks.items[i].id; 
				}
			}
			
			if(document.getElementById("artista").checked == true){
				this.numItems = this.info.artists.items.length;
				for(var i = 0; i < this.numItems; i++){
					this.artistID[i] = this.info.artists.items[i].name;
					var artistName = this.info.artists.items[i].name;
					a.innerHTML += "<div class='item'><li id='item" + i + "'>" + artistName + "</li> </div>";
				}
				MusicRecommender.search(document.getElementById('textSearch').value, 'track');
				this.numItems = this.info.tracks.items.length;
				for(var i = 0; i < this.numItems; i++){
					this.songURL[i] = this.info.tracks.items[i].preview_url;
					this.songName[i] = this.info.tracks.items[i].name;
				}
				
			}
			
			if(document.getElementById("album").checked == true){
	
				this.numItems = this.info.albums.items.length;

				for(var i = 0; i < this.numItems; i++){
					var albumName = this.info.albums.items[i].name;
					a.innerHTML += "<div class='item'><li id='item" + i + "'>" + albumName + "</li> </div>";
					//guardamos el id de cada album de la lista
					//this.albumID[i] = this.info.albums.items[i].id;
			
				}
			}
			
			document.getElementById('listaCanciones').appendChild(a);
		},
		
		detail:function(cancion, artista, album){

			document.getElementById('songName').innerHTML =  String(cancion);
			document.getElementById('artistName').innerHTML =  String(artista);
			if(album != ""){
				document.getElementById('albumName').innerHTML = String(album);
			}
		},
		
	
	}
	
	var Player = {
		musicPlayer : function(song){
			
			//document.getElementById('section').innerHTML = "";
			//En la variable idButton nos guardamos el id del link que nos han hecho click
			var idButton = song; 
			
			
			var music = document.getElementById('player');
			var idSong = MusicRecommender.getSongURL();
			music.src = idSong[2];
			
		}		
	}
	
	
	var Listener = {
		
		artistName : '',
		
		getArtistName : function(){
			return this.artistName;
		},
						
		searchButtonListener : function(){
			
			var btn = document.getElementById('buscador');

			/*btn.addEventListener("click", function (){
	    		//MusicRecommender.clearResults();
	    		var type = MusicRecommender.findSearchType();
	    		MusicRecommender.search(document.getElementById('textSearch').value, type);
	    		MusicRecommender.list();
				
				if (type == "artist" || type == "album"){

					ListenUserActions.ArtistListener(type);
				}else{
				
					ListenUserActions.playButtonListener("Cancion");
				}
				
			});*/

			document.onkeydown = function (enter){
				if(enter.keyCode === 13){
					//MusicRecommender.clearResults();
					var type = MusicRecommender.getType();
	    			MusicRecommender.search(document.getElementById('textSearch').value, type);
	    			MusicRecommender.list();
					Player.musicPlayer("p1");
					
					if (document.getElementById("artista").checked == true){
						Listener.showTracks();
						
					}
					
				}
 
			}
			
		},
		
		showTracks : function(){
			//var songURL = MusicRecommender.getSongURL();
			//var songName = MusicRecommender.getSongName();
			var artistas = MusicRecommender.getArtistID();
			var a = document.createElement('ul');
			
			if(document.getElementById("artista").checked == true){
					//var divArtist = document.getElementById('item'+i);
					var divArtist = document.getElementById('lista');
					divArtist.addEventListener('click', function(e){
						if(e.target && e.target.nodeName == "LI"){
							//limpiamos y mostramos tracks de ese artista
							document.getElementById('listaCanciones').removeChild(document.getElementById('lista'));
							MusicRecommender.search(e.target.textContent, 'track');
							var info = MusicRecommender.getInfo();
							for(var j=0; j < info.tracks.items.length ; j++){
								a.setAttribute("id", "lista");
								a.innerHTML += "<div class='item'><li id='item" + j + "'>" + info.tracks.items[j].name + "</li> <i class='fa fa-youtube-play' aria-hidden='true' fa-3x></i> </div>";
							}
							this.artistName = e.target.textContent;
							Listener.setDetails(this.artistName);	
						}
					});
				document.getElementById('listaCanciones').appendChild(a);
			}
			
			
		},
		
		setDetails : function(artista){		
			var divCancion = document.getElementById('lista');
			divCancion.addEventListener('click', function(e){
						if(e.target && e.target.nodeName == "LI"){
							//limpiamos y etc etc
							document.getElementById('listaCanciones').removeChild(document.getElementById('lista'));
							var songName = e.target.textContent;
							var artistNom = artista;
							//info.tracks.items[i].album.name
							MusicRecommender.search(artista, 'track');
							var info = MusicRecommender.getInfo();
							

							for(var i=0; i < info.tracks.items.length ; i++){
								if(info.tracks.items[i].name == songName){
									var albumName = info.tracks.items[i].album.name;
								}
							}
							
							MusicRecommender.detail(songName, artistNom, albumName);
						}
					});
		}
	}
	
	Listener.searchButtonListener();
	
	var buttonPlay = document.getElementById('buttonPlay');
			var player = document.getElementById('player');
				buttonPlay.addEventListener('click', function(){
					if(player.paused){
						player.play();
					}else{
						player.pause();
					}
				});
}
document.addEventListener("DOMContentLoaded",musicSalle(),false);